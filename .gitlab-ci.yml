stages:
  - build
  - test
  - quality
  - package
  - canary
  - deploy
  - performance

cache:
  paths:
    - .m2/repository
  key: "$CI_JOB_NAME"

variables:
  PLAYWD: ip172-18-0-5-cdno29m0qau000bves50

build_job:
  stage: build
  image: eclipse-temurin:11-jdk-alpine
  script:
    - ./mvnw compile
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true

test_job:
  stage: test
  image: eclipse-temurin:11-jdk-alpine
  script:
    - ./mvnw test
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true

code_quality_job:
  stage: quality
  allow_failure: true
  image: docker:stable
  services:
    - docker:stable-dind
  script:
    - mkdir codequality-results
    - docker run
        --env CODECLIMATE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        --volume /tmp/cc:/tmp/cc
        codeclimate/codeclimate analyze -f html > ./codequality-results/index.html
  artifacts:
    paths:
      - codequality-results/

sast_job:
  stage: quality
  artifacts:
    reports:
      sast: gl-sast-report.json
  image:
    name: returntocorp/semgrep:develop
  script:
    - semgrep scan --config auto --output gl-sast-report.json --json .

package_job:
  stage: package
  image: eclipse-temurin:11-jdk-alpine
  services:
    - docker:stable-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
  script:
    - apk add --no-cache docker openssh-client py3-pip python3-dev libffi-dev openssl-dev gcc libc-dev make
    - pip3 install docker-compose
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - ./mvnw install -PbuildDocker -DskipTests=true -DpushImage
      -Dhttps.protocols=TLSv1.2
      -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
      -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN
      -Dorg.slf4j.simpleLogger.showDateTime=true
      -Djava.awt.headless=true
      --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true
    - docker-compose build
    - docker-compose push

deploy_staging_job:
  stage: deploy
  image: docker:stable
  script:
    - apk add --no-cache openssh-client py-pip python-dev libffi-dev openssl-dev gcc libc-dev make
    - pip install docker-compose
    - export DOCKER_HOST=tcp://$PLAYWD.direct.labs.play-with-docker.com:2375
    - docker-compose down
    - docker-compose up -d
  environment:
    name: staging
    url: http://$PLAYWD-8080.direct.labs.play-with-docker.com

performance_job:
  stage: performance
  image: docker:git
  variables:
    URL: http://$PLAYWD-8080.direct.labs.play-with-docker.com/
  services:
    - docker:stable-dind
  script:
    - apk add --no-cache curl
    - x=1; while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' http://$PLAYWD-8080.direct.labs.play-with-docker.com/)" != "200" || $x -le 60 ]]; do sleep 5; echo $(( x++ )); done || false
    - mkdir gitlab-exporter
    - wget -O ./gitlab-exporter/index.js https://gitlab.com/gitlab-org/gl-performance/raw/master/index.js
    - mkdir sitespeed-results
    - docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:6.3.1 --plugins.add ./gitlab-exporter --outputFolder sitespeed-results $URL
    - mv sitespeed-results/data/performance.json performance.json
  artifacts:
    paths:
      - sitespeed-results/
    reports:
      performance: performance.json

deploy_prod_job:
  stage: deploy
  image: docker:stable
  script:
    - apk add --no-cache openssh-client py-pip python-dev libffi-dev openssl-dev gcc libc-dev make
    - pip install docker-compose
    - export DOCKER_HOST=tcp://$PLAYWD.direct.labs.play-with-docker.com:2375
    - docker-compose down
    - docker-compose up -d
  environment:
    name: prod
    url: http://$PLAYWD-8080.direct.labs.play-with-docker.com
  when: manual

canary_job:
  stage: canary
  image: docker:stable
  script:
    - apk add --no-cache openssh-client py-pip python-dev libffi-dev openssl-dev gcc libc-dev make
    - pip install docker-compose
    - export DOCKER_HOST=tcp://$PLAYWD.direct.labs.play-with-docker.com:2375
    - docker-compose down
    - docker-compose up -d
  environment:
    name: prod
    url: http://$PLAYWD-8080.direct.labs.play-with-docker.com
  when: manual
  only:
    - master
